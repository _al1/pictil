/*
Copyright (c) 2015/2016, al1
All rights reserved.
 
Thanks to Yann Guidon for some ideas to improve this code

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

1.	Redistributions of source code must retain the above copyright notice, this list of conditions 
	and the following disclaimer.
2.	Redistributions in binary form must reproduce the above copyright notice, this list of 
	conditions and the following disclaimer in the documentation and/or other materials provided 
	with the distribution.
3. 	Neither the name of the copyright holder nor the names of its contributors may be used to 
	endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
 
*/

#define _XTAL_FREQ 8000000

#include <xc.h>
#include <stdint.h>

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG
#pragma config FOSC = INTRC_IO  // Oscillator Selection (INTRC with I/O function on OSC2/CLKOUT and 10 us startup time)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT Disabled)
#pragma config CP = OFF         // Code Protection - User Program Memory (Code protection off)
#pragma config MCLRE = OFF      // Master Clear DISABLE (MCLR pin functions as RA3)
#pragma config IOSCFS = 8MHz    // Internal Oscillator Frequency Select (8 MHz INTOSC Speed)
#pragma config CPSW = OFF       // Code Protection - Self Writable Memory (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable (BOR Enabled)
#pragma config DRTEN = ON       // Device Reset Timer Enable (DRT Enabled (18 ms))

// This stores the font of the TIL311. font[4] gives the font for the number 4
/*    
  
 The hex input is on PORTA :
  bit 0 / A: RA4 LSBi
  bit 1 / B: RA5
  bit 2 / C: RA0
  bit 3 / D: RA1 MSBi         */

//remaped so it is adressed with ((portA&0x30)>>2)+(portA&0x03)
/*const uint16_t font[16] = {     //rev1 PCB
 ~(0x9473), // 0
 ~(0x24E7), // 4
 ~(0xB452), // 8
 ~(0xD074), // C
 ~(0x4407), // 1
 ~(0xF4E8), // 5
 ~(0xB443), // 9
 ~(0x94FB), // D
 ~(0xB09E), // 2
 ~(0xB470), // 6
 ~(0xA47F), // A
 ~(0xF0FC), // E
 ~(0xB48A), // 3
 ~(0xC487), // 7
 ~(0xB4FA), // B
 ~(0xE0F8)  // F
};*/
const uint16_t font[16] = {     //rev2 PCB
~(0x946B),	//0
~(0x24E7),	//4
~(0xB44A),	//8
~(0xD06C),	//C
~(0x4407),	//1
~(0xF4F0),	//5
~(0xB443),	//9
~(0x94FB),	//D
~(0xB09E),	//2
~(0xB468),	//6
~(0xA47F),	//A
~(0xF0FC),	//E
~(0xB492),	//3
~(0xC487),	//7
~(0xB4FA),	//B
~(0xE0F8)	//F
};

void main(void) 
{
    uint8_t  number=255, newNumber;
    uint16_t localFont;
    
    //set outputs
    TRISB = 0x0F;           // RB7..4 output
    TRISC = 0;              // RC7..0 output
    TRISA = 0xFB;           // RA2    output
    
    OPTION = 0xDF;          // diconnect TOCS from PA2
    ANSEL  = 0;             // no analog input;
    CM2CON0bits.C2ON = 0;   // disable compertor 2 to use C1..0 as output
    CM1CON0bits.C1ON = 0;
    
    while(1)
    {
        newNumber = PORTA & 0x33;
        if (number != newNumber)
          {
            number=newNumber;        
            if (PORTAbits.RA3 == 0) // if RA3 / latch strobe input == low
            {
                //display number
                localFont=font[((number&0x30)>>2)+ (number&0x03)];
                PORTC  = localFont;
                newNumber = localFont>>8;      // newNumber just reused as local variable
                PORTB  = newNumber;
                PORTA  = newNumber;
             }
          }
    }
    return;
}