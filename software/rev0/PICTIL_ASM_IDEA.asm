; PICTIL assembly code for the PIC16F527
; (c) Yann Guidon dim. nov. 29 03:31:14 CET 2015
;         version dim. nov. 29 05:00:02 CET 2015
; derived from code written by al1
; for https://hackaday.io/project/8270-pictil

; WARNING ! UNTESTED, INCOMPLETE, NOT EVEN EVER COMPILED, POSSIBLY DEEPLY FLAWED !

; Put configuration statements here...
; (I forgot the asm syntax)
; #pragma config FOSC = INTRC_IO  // Oscillator Selection (INTRC with I/O function on OSC2/CLKOUT and 10 us startup time)
; #pragma config WDTE = OFF       // Watchdog Timer Enable (WDT Disabled)
; #pragma config CP = OFF         // Code Protection - User Program Memory (Code protection off)
; #pragma config MCLRE = ON       // Master Clear Enable (MCLR pin functions as MCLR)
; #pragma config IOSCFS = 8MHz    // Internal Oscillator Frequency Select (8 MHz INTOSC Speed)
; #pragma config CPSW = OFF       // Code Protection - Self Writable Memory (Code protection off)
; #pragma config BOREN = ON       // Brown-out Reset Enable (BOR Enabled)
; #pragma config DRTEN = ON       // Device Reset Timer Enable (DRT Enabled (18 ms))

; map "number" to register 0xF

; Contents of the Flash data zone:

  ORG FLASH (?)
; LSB
  db ~(0x5F) // 0
  db ~(0xB4) // 2
  db ~(0x07) // 1
  db ~(0xA6) // 3
; MSB
  db ~(0xA0) // 0
  db ~(0xE4) // 2
  db ~(0x14) // 1
  db ~(0xE0) // 3

  ORG FLASH+16 (?)
; LSB
  db ~(0x56) // 8
  db ~(0x7F) // A
  db ~(0x47) // 9
  db ~(0xFE) // B
; MSB
  db ~(0xE0) // 8
  db ~(0xC4) // A
  db ~(0xE0) // 9
  db ~(0xE0) // B

  ORG FLASH+32 (?)
; LSB
  db ~(0xCF) // 4
  db ~(0x5A) // 6
  db ~(0xEA) // 5
  db ~(0x87) // 7
; MSB
  db ~(0x44) // 4
  db ~(0xE0) // 6
  db ~(0xF0) // 5
  db ~(0x94) // 7

  ORG FLASH+48 (?)
; LSB
  db ~(0x58) // C
  db ~(0xF8) // E
  db ~(0xFF) // D
  db ~(0xF8) // F
; MSB
  db ~(0xB4) // C
  db ~(0xF4) // E
  db ~(0xA0) // D
  db ~(0xD0) // F

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Code section ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; some jump code here

  ORG 5

  MOVLB 0

  movlw 0x0F
  TRIS PORTB

  movlw 0
  TRIS PORTC

  movlw 0xFB
  TRIS PORTA

  movlw 0xDF
  OPTION

  MOVLB 1
    clrf ANSEL
    bcf CM1CON0, 3
    bcf CM2CON0, 3
  MOVLB 0

; init
  movlw -1
  movwf number ; force display at first run

  movlw EEADR ; map newNumber to EEADR through INDF
  movwf FSR

MainLoop:
// Read the input:
    movlw 0x33
    andwf PORTA, w
    movwf INDF ; save the read value in the Flash address register

// Compare with last sample
    xorwf number, w
    btfsc STATUS, Z
      Goto MainLoop

; Scanning the input uses 6 instructions, 7 cycles,
; refresh is around 280KHz

    movf INDF, w
    movwf number

; Change the display:

    MOVLB 1
      BSF EECON, RD ; Trigger EE Read
      MOVF EEDATA, W
    MOVLB 0
    movwf PORTC

    movlw 4
    addwf INDF

    MOVLB 1
      BSF EECON, RD
      MOVF EEDATA, W
    MOVLB 0   
    movwf PORTB
    movwf PORTA

  goto MainLoop

; The end...


; The hex input is on PORTA :
;  bit 0: RA1
;  bit 1: RA0
;  bit 2: RA5
;  bit 3: RA4
;
;Sequence of increasing numbers, as seen from PORTA & 0x33:
;
;0x00
;0x02
;0x01
;0x03
;
;0x20
;0x22
;0x21
;0x23
;
;0x10
;0x12
;0x11
;0x13
;
;0x30
;0x32
;0x31
;0x33
;
;
; negated font array:
;const uint16_t font[16] = {
; ~(0xA05F), // 0
; ~(0x1407), // 1
; ~(0xE4B4), // 2
; ~(0xE0A6), // 3
; ~(0x44CF), // 4
; ~(0xF0EA), // 5
; ~(0xE05A), // 6
; ~(0x9487), // 7
; ~(0xE056), // 8
; ~(0xE047), // 9
; ~(0xC47F), // A
; ~(0xE0FE), // B
; ~(0xB458), // C
; ~(0xA0FF), // D
; ~(0xF4F8), // E
; ~(0xD0F8)  // F
;};
;
;Interleaving :
;
; ~(0xA05F), // 0
; ~(0xE4B4), // 2
; ~(0x1407), // 1
; ~(0xE0A6), // 3
;
; ~(0xE056), // 8
; ~(0xC47F), // A
; ~(0xE047), // 9
; ~(0xE0FE), // B
;
; ~(0x44CF), // 4
; ~(0xE05A), // 6
; ~(0xF0EA), // 5
; ~(0x9487), // 7
;
; ~(0xB458), // C
; ~(0xF4F8), // E
; ~(0xA0FF), // D
; ~(0xD0F8)  // F
;
;This maps well to the 64-bytes data flash area,
;LSB is at the direct address and MSB is at address+4?

