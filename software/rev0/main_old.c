/*
Copyright (c) 2015, al1
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

1.	Redistributions of source code must retain the above copyright notice, this list of conditions 
	and the following disclaimer.
2.	Redistributions in binary form must reproduce the above copyright notice, this list of 
	conditions and the following disclaimer in the documentation and/or other materials provided 
	with the distribution.
3. 	Neither the name of the copyright holder nor the names of its contributors may be used to 
	endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _XTAL_FREQ 8000000

#include <xc.h>
#include <stdint.h>

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG
#pragma config FOSC = INTRC_IO  // Oscillator Selection (INTRC with I/O function on OSC2/CLKOUT and 10 us startup time)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT Disabled)
#pragma config CP = OFF         // Code Protection - User Program Memory (Code protection off)
#pragma config MCLRE = ON       // Master Clear Enable (MCLR pin functions as MCLR)
#pragma config IOSCFS = 8MHz    // Internal Oscillator Frequency Select (8 MHz INTOSC Speed)
#pragma config CPSW = OFF       // Code Protection - Self Writable Memory (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable (BOR Enabled)
#pragma config DRTEN = ON       // Device Reset Timer Enable (DRT Enabled (18 ms))

// This stores the font of the TIL311. font[4] gives the font for the number 4
/* the bits are ordered like this:

	BitPos:		15  14  13  12  11  10   9   8 |  7   6   5   4   3   2   1   0
	LED#:	     2  13   8   3   X   7   X   X |  1  12   9  10  11   4   6   5
	PIC pin#:   RB7 RB6 RB5 RB4  -  RA2  -   - | RC7 RC6 RC5 RC4 RC3 RC2 RC1 RC0
											   |										   
	LED number in TIL311 number:
	
	 1   2  3
	12      4
	11  13  5
	10      6
	 9   8  7           */
const uint16_t font[16] = 
//       0      1       2       3       4       5       6       7
    { 0xA05F, 0x1407, 0xE4B4, 0xE0A6, 0x44CF, 0xF0EA, 0xE05A, 0x9487, 
      0xE056, 0xE047, 0xC47F, 0xE0FE, 0xB458, 0xA0FF, 0xF4F8, 0xD0F8 };
//       8      9       A       B       C       D       E       F

// function to display a number 
void displayNumber(uint8_t number)
{
    uint16_t localFont=~font[number];
    
    //reset all outputs
    PORTB &= 0x0F;
    PORTC  = 0;
    PORTA &= 0xFB;
    
    //display number
    PORTB |= (localFont>>8)&0xF0;
    PORTC |= localFont;
    PORTA |= (localFont>>8)&0x04;    
}

void main(void) 
{
    uint8_t counter=0;
    uint8_t number=0, newNumber=0;
    
    //set outputs
    TRISB = 0x0F;           // RB7..4 output
    TRISC = 0;              // RC7..0 output
    TRISA = 0xFB;           // RA2    output
    
    OPTION = 0xDF;          // diconnect TOCS from PA2
    ANSEL  = 0;             // no analog input;
    CM2CON0bits.C2ON = 0;   // disable compertor 2 to use C1..0 as output
    CM1CON0bits.C1ON = 0;
    
    
    displayNumber(0);
    
    while(1)
    {
        newNumber=(PORTAbits.RA4<<3)+(PORTAbits.RA5<<2)+(PORTAbits.RA0<<1)+PORTAbits.RA1;
        if (number != newNumber)
          {
            displayNumber (newNumber);
            number=newNumber;
          }
        //__delay_ms(20);
    }
    return;
}
